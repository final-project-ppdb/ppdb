# Final Project

# Kelompok 8

# Anggota Kelompok

<ul>
    <li>Dio Atha Shalsabilla</li>
    <li>Eradika Reza Lutfiansyah</li>
    <li>Ahmad Busthomi</li>
</ul>

# Tema project

<p>PPDB (Penerimaan Peserta Didik Baru)</p>

# ERD

<img src="public/erd.jpg"/>

# Link Aplikasi

<p>Link Demo Aplikasi : [https://youtu.be/81id6DrOMzk]</p>
<p>Link Deploy Aplikasi : [https://alamedia.my.id]</p>
