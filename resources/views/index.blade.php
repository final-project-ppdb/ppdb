@extends('layout.master')
@section('title','Dashboard')
@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <style>
    .jumbotron {
      background-image: url("https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=1920,fit=crop/AzGRxyegrGSGN2G3/BACKGROUND-mxB3a4x0WvHvz2Kd.png");
      background-size: cover;
    }
  </style>
  <div class="jumbotron">
    <h1 class="display-4">Selamat Datang!</h1>
    <p class="lead">di PPDB Dinas Pendidikan Provinsi DKI Jakarta</p>
    <hr class="my-4">
    <p>Situs ini dipersiapkan sebagai pengganti pusat informasi dan pengolahan seleksi data siswa peserta PPDB Provinsi DKI Jakarta Periode 2022 / 2023 secara online real time process untuk pelaksanaan PPDB Online.</p>
    <p class="lead">
      @guest
      <a class="btn btn-primary btn-lg" href="{{ route('login') }}" role="button">Log In</a>
      @endguest
    </p>
  </div>
</div>

  <!-- /.content --> 
    
@endsection