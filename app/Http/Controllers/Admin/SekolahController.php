<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Sekolah;
use Illuminate\Http\Request;

class SekolahController extends Controller
{
    public function index()
    {
        $sekolah = Sekolah::all();
        return view('admin.sekolah.index', compact('sekolah'));
    }

    public function create()
    {
        return view("admin.sekolah.create");
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required|unique:sekolah,nama',
        ], [
            'nama.required' => 'Nama sekolah tidak boleh kosong',
            'nama.unique' => 'Nama sekolah sudah ada',
        ]);
        $sekolah = new Sekolah;

        $sekolah->nama = $request->nama;

        $sekolah->save();

        return redirect('/sekolah')->with('sukses', 'Data berhasil ditambahkan');
    }

    public function show($sekolah_id)
    {
        $sekolah = Sekolah::where('id', $sekolah_id)->first();
        return view('admin.sekolah.show', compact('sekolah'));
    }

    public function edit($sekolah_id)
    {
        $sekolah = Sekolah::where('id', $sekolah_id)->first();
        return view('admin.sekolah.edit', compact('sekolah'));
    }

    public function update(Request $request, $sekolah_id)
    {
        $validated = $request->validate([
            'nama' => 'required|unique:sekolah,nama',
        ], [
            'nama.required' => 'Nama sekolah tidak boleh kosong',
            'nama.unique' => 'Nama sekolah sudah ada',
        ]);
        $sekolah = Sekolah::find($sekolah_id);

        $sekolah->nama = $request['nama'];

        $sekolah->save();

        return redirect('/sekolah')->with('suksesEdit', 'Data berhasil di update');;
    }

    public function destroy($sekolah_id)
    {
        $sekolah = Sekolah::find($sekolah_id);
        $sekolah->delete();

        return redirect('/sekolah')->with('delete', 'Data berhasil dihapus');;
    }
}
