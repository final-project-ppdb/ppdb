<?php

namespace App\Http\Controllers\Siswa;

use App\Http\Controllers\Controller;
use App\Pendaftar;
use App\Persyaratan;
use App\Sekolah;
use App\Tentang;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PDF;

class SiswaController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $pendaftar = Pendaftar::where('user_id', $id)->first();
        $sekolah = Sekolah::all();

        return view('siswa.form', compact('sekolah', 'pendaftar'));
    }

    public function formulir(Request $request)
    {
        $request->validate([
            'nik' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'provinsi' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'ipa' => 'required',
            'bhs_indo' => 'required',
            'bhs_inggris' => 'required',
            'matematika' => 'required',
            'sekolah_id' => 'required',
            'no_telp' => 'required',
            'foto' =>  'required|mimes:jpg,png|max:2048',
            'ijasah' =>  'required|mimes:jpg,png|max:2048',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'alamat_ortu' => 'required',
            'no_hp_ortu' => 'required',
            'pekerjaan_ayah' => 'required',
            'pekerjaan_ibu' => 'required',
            'penghasilan_ortu' => 'required',
        ]);
        // if ($request->) {
        //     # code...
        // }
        $foto = $request->file('foto')->store('foto');
        $ijasah = $request->file('ijasah')->store('ijasah');
        $id = auth()->user()->id;

        Pendaftar::where('user_id', $id)
            ->update([
                'nik' => $request->nik,
                'jenis_kelamin' => $request->jenis_kelamin,
                'alamat' => $request->alamat,
                'kecamatan' => $request->kecamatan,
                'kabupaten' => $request->kabupaten,
                'provinsi' => $request->provinsi,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'ipa' => $request->ipa,
                'bhs_indo' => $request->bhs_indo,
                'bhs_inggris' => $request->bhs_inggris,
                'matematika' => $request->matematika,
                'sekolah_id' => $request->sekolah_id,
                'no_telp' => $request->no_telp,
                'foto' => $foto,
                'ijasah' => $ijasah,
                'nama_ayah' => $request->nama_ayah,
                'nama_ibu' => $request->nama_ibu,
                'alamat_ortu' => $request->alamat_ortu,
                'no_hp_ortu' => $request->no_hp_ortu,
                'pekerjaan_ayah' => $request->pekerjaan_ayah,
                'pekerjaan_ibu' => $request->pekerjaan_ibu,
                'penghasilan_ortu' => $request->penghasilan_ortu,
                'status' => 1,
            ]);

        return redirect(route('siswa.formulir'))->with('sukses', 'Data berhasil di kirim slihkan menggunggu pengumuman');
    }
    public function detailpendaftar($id)
    {
        // $pendaftar = 

        // return view('siswa.detail', compact('pendaftar'));
    }

    public function pengumuman(Pendaftar $pen)
    {
        $id = Auth::user()->id;
        $pendaftar = Pendaftar::where('user_id', $id)->first();
        return view('siswa.pengumuman', compact('pendaftar'));
    }

    public function ubahpassword()
    {
        return view('siswa.ubahpassword');
    }

    public function updateubahpassword(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'ulangi_password' => 'required|same:password',
        ]);

        $id = Auth::user()->id;
        User::where('id', $id)
            ->update([
                'password' => Hash::make($request->password)
            ]);

        return redirect(route('siswa.ubahpassword'))->with('sukses', 'Password Berhasil Di Ubah');
    }

    public function cetakformulir()
    {
        $id = Auth::user()->id;
        $pendaftar = Pendaftar::where('user_id', $id)->first();

        $pdf = PDF::loadView('siswa.cetakformulir', compact('pendaftar'));
        return $pdf->download('Daftar-Ulang');
    }

    public function persyaratan()
    {
        $persyaratan = Persyaratan::all();
        return view('siswa.persyaratan', compact('persyaratan'));
    }

    public function tentang()
    {
        $tentang = Tentang::all();
        return view('siswa.tentang', compact('tentang'));
    }
}
