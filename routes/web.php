<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Siswa\SiswaController;
use Illuminate\Support\Facades\Route;




Route::get('/', 'DashboardController@index')->name('index');
Route::get('/login', 'Auth\AuthController@login')->name('login');
Route::post('/login', 'Auth\AuthController@proseslogin')->name('proseslogin');
Route::get('/register',  'Auth\AuthController@register')->name('register');
Route::post('/register', 'Auth\AuthController@prosesregister')->name('prosesregister');
Route::post('/logout', 'Auth\AuthController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:1']], function () {
        Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');
        Route::get('/ubahpassword', 'Admin\AdminController@ubahpassword')->name('admin.ubahpassword');
        Route::post('/updateubahpassword', 'Admin\AdminController@updateubahpassword')->name('admin.updateubahpassword');


        //route roles
        Route::get('/roles', 'Roles\RolesController@index')->name('roles.index');
        Route::get('/roles/create', 'Roles\RolesController@create')->name('roles.create');
        Route::post('/roles', 'Roles\RolesController@store')->name('roles.store');
        Route::get('/roles/{id}/edit', 'Roles\RolesController@edit')->name('roles.edit');
        Route::put('/roles/{id}', 'Roles\RolesController@update')->name('roles.update');
        Route::delete('/roles/{id}', 'Roles\RolesController@destroy')->name('roles.delete');

        //route CRUD sekolah    
        Route::get('/sekolah/create', 'Admin\SekolahController@create')->name('sekolah.create');
        Route::post('/sekolah', 'Admin\SekolahController@store')->name('sekolah.store');
        Route::get('/sekolah', 'Admin\SekolahController@index')->name('sekolah.index');
        Route::get('/sekolah/{sekolah_id}', 'Admin\SekolahController@show')->name('sekolah.show');
        Route::get('/sekolah/{sekolah_id}/edit', 'Admin\SekolahController@edit')->name('sekolah.edit');
        Route::put('/sekolah/{sekolah_id}', 'Admin\SekolahController@update')->name('sekolah.update');
        Route::delete('/sekolah/{sekolah_id}', 'Admin\SekolahController@destroy')->name('sekolah.delete');

        //route pendaftar
        Route::get('/pendaftar/view', 'Admin\PendaftarController@index')->name('pendaftar.index');
        Route::get('/pendaftar/detail/{id}', 'Admin\PendaftarController@detail')->name('pendaftar.detail');
        Route::post('/pendaftar/diterima/{id}', 'Admin\PendaftarController@updateterima')->name('pendaftar.updateterima');
        Route::post('/pendaftar/ditolak/{id}', 'Admin\PendaftarController@updatetolak')->name('pendaftar.updatetolak');
        Route::get('/pendaftar/diterima', 'Admin\PendaftarController@terima')->name('pendaftar.terima');
        Route::get('/pendaftar/ditolak', 'Admin\PendaftarController@tolak')->name('pendaftar.tolak');

        //route pengguna
        Route::get('/pengguna', 'Admin\PenggunaController@index')->name('pengguna.index');
        Route::get('/pengguna/{id}', 'Admin\PenggunaController@edit')->name('pengguna.edit');
        Route::post('/pengguna/{id}/edit', 'Admin\PenggunaController@update')->name('pengguna.update');

        //route persyaratan 
        Route::get('/persyaratan/create', 'Admin\PersyaratanController@create')->name('persyaratan.create');
        Route::post('/persyaratan', 'Admin\PersyaratanController@store')->name('persyaratan.store');
        Route::get('/persyaratan/view', 'Admin\PersyaratanController@index')->name('persyaratan.index');
        Route::get('/persyaratan/{id}', 'Admin\PersyaratanController@show')->name('persyaratan.show');
        Route::get('/persyaratan/{id}/edit', 'Admin\PersyaratanController@edit')->name('persyaratan.edit');
        Route::put('/persyaratan/{id}', 'Admin\PersyaratanController@update')->name('persyaratan.update');
        Route::delete('/persyaratan/{id}', 'Admin\PersyaratanController@destroy')->name('persyaratan.delete');

        //route tentang 
        Route::get('/tentang/create', 'Admin\TentangController@create')->name('tentang.create');
        Route::post('/tentang', 'Admin\TentangController@store')->name('tentang.store');
        Route::get('/tentang/view', 'Admin\TentangController@index')->name('tentang.index');
        Route::get('/tentang/{id}', 'Admin\TentangController@show')->name('tentang.show');
        Route::get('/tentang/{id}/edit', 'Admin\TentangController@edit')->name('tentang.edit');
        Route::put('/tentang/{id}', 'Admin\TentangController@update')->name('tentang.update');
        Route::delete('/tentang/{id}', 'Admin\TentangController@destroy')->name('tentang.delete');
    });
    Route::group(['middleware' => ['cek_login:2']], function () {
        Route::get('/form', 'Siswa\SiswaController@index')->name('siswa.index');
        Route::post('/form', 'Siswa\SiswaController@formulir')->name('siswa.formulir');
        Route::get('/detailpendaftar', 'Siswa\SiswaController@detailpendaftar')->name('siswa.detail');
        Route::get('/pengumuman', 'Siswa\SiswaController@pengumuman')->name('siswa.pengumuman');
        Route::get('/cetakformulir', 'Siswa\SiswaController@cetakformulir')->name('siswa.cetakformulir');


        Route::get('/ubahpassword', 'Siswa\SiswaController@ubahpassword')->name('siswa.ubahpassword');
        Route::post('/updateubahpassword', 'Siswa\SiswaController@updateubahpassword')->name('siswa.updateubahpassword');
    });
});



Route::get('/persyaratan', 'Siswa\SiswaController@persyaratan')->name('siswa.persyaratan');
Route::get('/tentang', 'Siswa\SiswaController@tentang')->name('siswa.tentang');
